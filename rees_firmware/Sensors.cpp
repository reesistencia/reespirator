/** Sensors.
 *
 * @file Sensors.cpp
 *
 * This is the Sensors module.
 * It handles the Sensors readings and storage.
 */

#include "Sensors.h"

/*!
 *  @brief  Sensor Class Constructor. Initialize pressure and volume sensors
 */
Sensors::Sensors(void)
{
    _init();
}

/*!
 *  @brief  Initialize pressure and volume sensors
 */
void Sensors::_init()
{
    _abp = new Honeywell_ABP(
        0x28,   // I2C address
        0,      // minimum pressure
        70.307, // maximum pressure
        "mbar"  // pressure unit (1 mbar = 1 cmH2O = 1 hPa)
    );

    _errorCounter = 0;
    _state = SensorStateFailed;

    #if ENABLED_SENSOR_VOLUME_SFM3300
        _sfm3000 = new SFM3000wedo(64);
        _sfm3000->init();
    #endif

    #if ENABLED_SENSOR_VOLUME_SDP800
        _sdp = new SDP8XXSensor();
        if (_sdp->init())
        {
            _sdp->startContMeasure();
        }
        else
        {
            Serial.println(F("SDP FAILED!"));
            _volumeState = SensorStateFailed;
        }
    #endif

    #if ENABLED_SENSOR_VOLUME
        resetVolumeIntegrator();
    #endif
}

#if ENABLED_SENSOR_VOLUME_SFM3300
/*!
 *  @brief  Reboot SFM3300 sensor and reset volume integrator
 */
void Sensors::rebootVolumeSensor()
{
    _sfm3000->reset();
    _sfm3000->init();
    resetVolumeIntegrator();
}
#endif

/**
 * @brief Update pressure values from sensor
 */
void Sensors::readPressure()
{
    float pres1;
    // Acquire sensors data
    _abp->update();
    pres1 = _abp->pressure();
    _pressure1 = pres1;
    if (_minPressure > _pressure1)
    {
        _minPressure = _pressure1;
    }
    if (_maxPressure < _pressure1)
    {
        _maxPressure = _pressure1;
    }
}

/**
 * @brief Get relative pressure in cmH2O
 *
 * @note  Pressure units defined in Honeywell_ABP init
 *
 * @return SensorValues_t - pressure value
 */
SensorPressureValues_t Sensors::getRelativePressure()
{
    SensorPressureValues_t values;
    values.pressure1 = _pressure1;
    values.state = SensorStateOK;
    return values;
}

/**
 * @brief Get last max and min pressure measures
 *
 * @return Returns SensorLastPressure_t
 */
SensorLastPressure_t Sensors::getLastPressure(void)
{
    SensorLastPressure_t lastPres;
    lastPres.minPressure = _lastMinPressure;
    lastPres.maxPressure = _lastMaxPressure;
    return lastPres;
}

/**
 * @brief Reset cycle max and min pressure values
 */
void Sensors::resetPressures(void)
{
    _minPressure = 255;
    _maxPressure = 0;
}

#if ENABLED_SENSOR_VOLUME
/**
 * @brief Get last read flow value (not updated)
 *
 * @return flow
 */
float Sensors::getFlow(void)
{
    return _flow;
}

/**
 * @brief Update flow values from sensor
 */
void Sensors::readFlow(void)
{
#if ENABLED_SENSOR_VOLUME_SFM3300
    SFM3000_Value_t tmp = _sfm3000->getvalue(); //TODO crc
    if (tmp.crcOK)
    {
        _volumeState = SensorStateOK;
    }
    else
    {
        _volumeState = SensorStateFailed;
    }
    float flow = ((float)tmp.value - SFM3300_OFFSET) / SFM3300_SCALE; // lpm

    // Record flow samples
    if (flow == _flow && (flow == 10914 || flow == -10197))
    {
        _flowRepetitionCounter++;
    }
    else
    {
        _flowRepetitionCounter = 0;
    }
    _flow = flow;
#endif
#if ENABLED_SENSOR_VOLUME_SDP800

    _diffPressure = _sdp->getContPressure(); // Pa

#endif
#if ENABLED_PITOT_METER

    _flow = sign(_diffPressure) * K_PITOT_FLOW_LPM_PRESSURE_PA * sqrt(abs(_diffPressure)); // lpm

#endif
}

/**
 * @brief Get last calculated volume values
 *
 * @return Returns SensorVolumeValue_t
 */
SensorVolumeValue_t Sensors::getVolume()
{
    SensorVolumeValue_t values;
    values.state = _volumeState;
    values.volume = _lastVolume;
    values.instantVolume = _volume_ml;
    return values;
}

/**
 * @brief Update volume count value
 */
void Sensors::integrateVolume(void)
{
    unsigned long now = millis();
    unsigned short mseconds = (unsigned short)(now - _lastReadFlow);
    float ml = _flow * mseconds / 60; // l/min * ms * 1000 (ml) /60000 (ms)
    _volume_ml += ml;
    Serial.println(_volume_ml);
    _lastReadFlow = now;
}

/**
 * @brief Reset integrated volume from flow readings
 */
void Sensors::resetVolumeIntegrator(void)
{
    _volume_ml = 0;
    _lastReadFlow = millis();
}

/**
 * @brief Store volume and limit pressures
 */
void Sensors::saveVolume(void)
{
    _lastVolume = _volume_ml;
    _lastMinPressure = _minPressure;
    _lastMaxPressure = _maxPressure;
}

/**
 * @brief Check if volume keeps sending an exact constant value
 */
bool Sensors::stalledVolumeSensor()
{
    if (_flowRepetitionCounter > 10)
    {
        _flowRepetitionCounter = 0;
        return true;
    }
    return false;
}

#endif
