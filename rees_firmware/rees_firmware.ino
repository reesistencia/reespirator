/**
 * @file rees_firmware.ino
 * @author Reesistencia Team Spain http://reesistencia.org/
 * @brief This firmware controls Reespirator 24, a Mapleson-A mechanical ventilation.
 * @version 0.1.1 - PCV/VCV reespirator 24
 * @date 2020-04-27
 * @copyright GPLv3
 */

/**
 * Dependencies
 */

#include "defaults.h"
#include "calc.h"
#include "Sensors.h"
#include "MechVentilation.h"
#include "Alarms.h"

#include "src/AutoPID/AutoPID.h"
#include "src/FlexyStepper/FlexyStepper.h"
#include "src/TimerOne/TimerOne.h"
#include "src/TimerThree/TimerThree.h"
#include <avr/wdt.h>

/**
 * Variables
 */

FlexyStepper *stepper;
Sensors *sensors;
AutoPID *pid;
MechVentilation *ventilation;
Alarms *alarms;

VentilationOptions_t options;

volatile bool flagAlarm = false;
volatile unsigned long timeLastAlarm = 0;
volatile unsigned char timer1Counter = 0;
volatile unsigned long A;

/**
 * Setup
 */

void setup()
{
    /* #region Ventilation options */
    options.height = DEFAULT_HEIGHT;
    options.sex = DEFAULT_SEX;
    options.respiratoryRate = DEFAULT_RPM;
    options.peakInspiratoryPressure = DEFAULT_PEAK_INSPIRATORY_PRESSURE;
    options.peakEspiratoryPressure = DEFAULT_PEAK_ESPIRATORY_PRESSURE;
    options.triggerThreshold = DEFAULT_TRIGGER_THRESHOLD;
    options.hasTrigger = false;
    /* #endregion */

    // Power on BeagleBone
    pinMode(PIN_POWER_BEAGLE, INPUT_PULLUP);
    if (!digitalRead(PIN_POWER_BEAGLE))
    {
        pinMode(PIN_POWER_BEAGLE, OUTPUT);
        digitalWrite(PIN_POWER_BEAGLE, HIGH);
        delay(250);
        digitalWrite(PIN_POWER_BEAGLE, LOW);
        delay(250);
        digitalWrite(PIN_POWER_BEAGLE, HIGH);
    }

    // Watchdog
    wdt_disable();
    wdt_enable(WDTO_8S);

    // Serial and comms
    Serial.begin(115200);
    Serial2.begin(115200);
    Serial.println(F("Setup"));
    Serial2.println(F("Setup"));
    Wire.begin();

    // Buzzer
    pinMode(PIN_BUZZ, OUTPUT);
    digitalWrite(PIN_BUZZ, HIGH);
    delay(100);
    digitalWrite(PIN_BUZZ, LOW);

    // Homing magnetic sensor
    // signal is HIGH when homing is detected
    pinMode(PIN_STEPPER_ENDSTOP, INPUT_PULLUP);

    // Solenoid
    pinMode(PIN_SOLENOID, OUTPUT);

    // Sensors
    #if ENABLED_SENSOR_VOLUME_SFM3300
        pinMode(20, INPUT_PULLUP);
        pinMode(21, INPUT_PULLUP);
    #endif

    sensors = new Sensors();

    alarms = new Alarms(sensors);

    // PID
    pid = new AutoPID(PID_MIN, PID_MAX, PID_PRESSURE_KP, PID_PRESSURE_KI, PID_PRESSURE_KD);
    // if pressure is more than PID_BANGBANG below or above setpoint,
    // output will be set to min or max respectively
    pid->setBangBang(PID_BANGBANG);
    // set PID update interval
    pid->setTimeStep(PID_TS);

    /* #region Stepper configuration */
    stepper = new FlexyStepper();

    // connect and configure the stepper motor to its IO pins
    stepper->connectToPins(PIN_STEPPER_STEP, PIN_STEPPER_DIRECTION, STEPPER_DRIVER_INVERTED_LOGIC);
    stepper->setStepsPerRevolution(STEPPER_STEPS_PER_REVOLUTION * STEPPER_MICROSTEPS);
    pinMode(PIN_STEPPER_EN, OUTPUT);
    digitalWrite(PIN_STEPPER_EN, LOW);

    /** @todo Relay and stepper driver alarm */
    // pinMode(PIN_RELAY, OUTPUT);
    // digitalWrite(PIN_RELAY, LOW);
    // pinMode(PIN_STEPPER_ALARM, INPUT_PULLUP);
    /* #endregion */

    ventilation = new MechVentilation(
        stepper,
        sensors,
        pid,
        options);

    // Find homing and wait
    ventilation->start();
    ventilation->update();
    ventilation->stop();
    delay(1000);

    sensors->readPressure();

    /* #region  Timers */
    Timer3.initialize(TIME_STEPPER_ISR_MICROS);
    Timer3.attachInterrupt(timer3Isr);
    Timer1.initialize(TIME_BASE_MICROS);
    Timer1.attachInterrupt(timer1Isr);
    /* #endregion */
}

/**
 * Loop
 */

void loop()
{
    unsigned long time;
    time = millis();
    unsigned long static lastReadSensor = 0;
    unsigned long static lastComms = 0;
    unsigned long static lastSendConfiguration = 0;
    State static lastState;

    if (time > lastSendConfiguration + TIME_SEND_CONFIGURATION)
    {
        wdt_reset();
        Serial2.print(F("CONFIG "));
        Serial2.print(ventilation->getPeakInspiratoryPressure());
        Serial2.print(F(" "));
        Serial2.print(ventilation->getPeakEspiratoryPressure());
        Serial2.print(F(" "));
        Serial2.print(ventilation->getRPM());
        Serial2.print(F(" "));
        Serial2.print(ventilation->getIE());
        Serial2.print(F(" "));
        Serial2.print(ventilation->getTriggerThreshold());
        Serial2.print(F(" "));
        Serial2.print(ventilation->getMode());
        Serial2.print(F(" "));
        Serial2.print(ventilation->getControl());
        Serial2.print(F(" "));
        Serial2.print(ventilation->getTidalVolume());
        Serial2.print(F(" "));
        Serial2.print(ventilation->getFlow());
        Serial2.print(F(" "));
        Serial2.println((int)ventilation->getStartStop());
        wdt_reset();
        lastSendConfiguration = time;

        #if ENABLED_SENSOR_VOLUME_SFM3300
            if (sensors->stalledVolumeSensor() && ventilation->getStartStop())
            {
                // Serial2.println("Rebooting");
                sensors->rebootVolumeSensor();
            }
        #endif
    }

    if (time > lastReadSensor + TIME_SENSOR)
    {
        sensors->readFlow();
        sensors->readPressure();
        sensors->integrateVolume();
        lastReadSensor = time;
    }

    if (flagAlarm)
    {

        digitalWrite(PIN_SOLENOID, SOLENOID_OPEN);
        //digitalWrite(PIN_RELAY, HIGH);
        //delayMicroseconds(60000);
        //delayMicroseconds(60000);
        //delayMicroseconds(60000);
        //digitalWrite(PIN_RELAY, LOW);
        delayMicroseconds(60000);
        delayMicroseconds(60000);
        delayMicroseconds(60000);
        //ventilation->setState(State_Homing);
        //digitalWrite(PIN_BUZZ, HIGH); // test zumbador
        delayMicroseconds(60000);
        //digitalWrite(PIN_BUZZ, LOW);
        //detachInterrupt(digitalPinToInterrupt(PIN_STEPPER_ALARM));
        flagAlarm = false;
    }

    if (time > lastComms + TIME_COMMS)
    {
        SensorVolumeValue_t volume = sensors->getVolume();
        SensorPressureValues_t pressure = sensors->getRelativePressure();

        char string[100];
        long tmpFlow = (long)(sensors->getFlow() * 1000.0F);
        long tmpVolume = (long)(volume.instantVolume);
        sprintf(string, "DT %05d %07ld %07ld %01d", ((int)pressure.pressure1), tmpFlow, tmpVolume, ((int)alarms->readAlarm()));
        Serial2.println(string);

        if (alarms->getMutingExpiredFlag())
        {
            Serial2.println(F("MUTEXP"));
            alarms->setMutingExpiredFlag(false);
        }

        if (pressure.state == SensorStateFailed)
        {
            /** @todo sensor fail. buzz alarms */
            Serial2.println(F("FALLO Sensor"));
        }
        if (volume.state == SensorStateFailed)
        {
            Serial2.println("Fallo CRC sensor volumen");
        }
        lastComms = time;

        /*
         * Notify insufflated volume
         */
        if (ventilation->getState() != lastState)
        {
            SensorLastPressure_t lastPressure = sensors->getLastPressure();
            if (ventilation->getState() == Init_Exsufflation)
            {
                Serial2.println("EOC " + String(lastPressure.maxPressure) + " " +
                                String(lastPressure.minPressure) + " " + String(volume.volume));
                sensors->resetPressures();
                sensors->resetVolumeIntegrator();
            }
            else if (ventilation->getState() == State_Exsufflation)
            {
                if (lastState != Init_Exsufflation)
                {
                    Serial2.println("EOC " + String(lastPressure.maxPressure) + " " +
                                    String(lastPressure.minPressure) + " " + String(volume.volume));
                    sensors->resetPressures();
                    sensors->resetVolumeIntegrator();
                }
            }
        }
        lastState = ventilation->getState();
    }

    if (Serial2.available())
    {
        readIncomingMsg();
    }
}

void timer1Isr(void)
{
    ventilation->update();
    alarms->update(ventilation->getPeakInspiratoryPressure());
}

void timer3Isr(void)
{
    stepper->processMovement();
}

void driverAlarmIsr(void)
{
    if (millis() > timeLastAlarm + 5000)
    {
        flagAlarm = true;
        timeLastAlarm = millis();
    }
}

/**
 * Read commands
 */

void readIncomingMsg(void)
{
    char msg[100];
    Serial2.readStringUntil('\n').toCharArray(msg, 100);
    int pip, peep, fr, triggerThreshold, ie, flow, tidalVolume;
    if (String(msg).substring(0, 6) == "CONFIG")
    {
        int rc = sscanf(msg, "CONFIG PIP %d", &pip);
        if (rc == 1)
        {
            ventilation->setPeakInspiratoryPressure(pip);
            return;
        }

        rc = sscanf(msg, "CONFIG PEEP %d", &peep);
        if (rc == 1)
        {
            ventilation->setPeakEspiratoryPressure(peep);
            return;
        }

        rc = sscanf(msg, "CONFIG BPM %d", &fr);
        if (rc == 1)
        {
            ventilation->setRPM(fr);
            return;
        }

        rc = sscanf(msg, "CONFIG TRIGTH %d", &triggerThreshold);
        if (rc == 1)
        {
            ventilation->setTriggerThreshold(triggerThreshold);
            return;
        }

        rc = sscanf(msg, "CONFIG IE %d", &ie);
        if (rc == 1)
        {
            ventilation->setIE(ie);
            return;
        }

        rc = sscanf(msg, "CONFIG FLOW %d", &flow);
        if (rc == 1)
        {
            ventilation->setFlow(flow);
            return;
        }

        rc = sscanf(msg, "CONFIG VT %d", &tidalVolume);
        if (rc == 1)
        {
            ventilation->setTidalVolume(tidalVolume);
            return;
        }
    }
    else if (String(msg).substring(0, 7) == "RECRUIT")
    {
        uint8_t tmp = 255;
        int rc = sscanf(msg, "RECRUIT %d", &tmp);
        switch (tmp)
        {
        case 0:
            Serial2.println("ACK 0");
            ventilation->deactivateRecruitment();
            break;
        case 1:
            Serial2.println("ACK 1");
            ventilation->activateRecruitment();
            break;
        default:
            break;
        }
    }
    else if (String(msg).substring(0, 7) == "TRIGGER")
    {
        uint8_t tmp = 255;
        int rc = sscanf(msg, "TRIGGER %d", &tmp);
        switch (tmp)
        {
        case 0:
            Serial2.println("ACK 0");
            ventilation->setMode(Controlled_Mode);
            break;
        case 1:
            Serial2.println("ACK 1");
            ventilation->setMode(Assisted_Mode);
            break;
        default:
            break;
        }
    }
    else if (String(msg).substring(0, 4) == "MUTE")
    {
        uint8_t tmp = 255;
        int rc = sscanf(msg, "MUTE %d", &tmp);
        switch (tmp)
        {
        case 0:
            Serial2.println("ACK 0");
            // TODO: Unmute alarms
            break;
        case 1:
            Serial2.println("ACK 1");
            // TODO: Mute alarms
            break;
        default:
            break;
        }
    }
    else if (String(msg).substring(0, 3) == "PCV")
    {
        ventilation->setControl(Pressure_Control);
    }
    else if (String(msg).substring(0, 3) == "VCV")
    {
        ventilation->setControl(Volume_Control);
    }
    else if (String(msg).substring(0, 5) == "START")
    {
        ventilation->start();
        alarms->start();
        Serial2.println("ACK START");
    }
    else if (String(msg).substring(0, 4) == "STOP")
    {
        ventilation->stop();
        alarms->stop();
        Serial2.println("ACK STOP");
    }
}
