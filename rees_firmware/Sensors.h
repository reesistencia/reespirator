/** Sensors.
 *
 * @file Sensors.h
 *
 * This is the Sensors module.
 * It handles the Sensors readings and storage.
 *
 * @section intro_sec Introduction
 *
 *  Library for the management of **Reespirator24** sensors.
 *
 *  - **Honeyell ABP L(Leadless SMT) AN(Single axialbarbed port) T(Liquid media, no diagnostics) 001PG(0psi to 1psi) 2(I2C, Address 0x28) A 5(5.0 Vdc) Sensor**
 *      Main Line Pressure Sensor from Honeyell Co. See specific library: @see "src/Honeywell_ABP.h"
 *  - **Sensirion DP810-125Pa Sensor**
 *      Differencial Pressure Sensor from Sensirion AG for Pitot/Venturi Tube FlowMeter Sensor. Custom library: @see "src/sdpsensor.h"
 *  - **Sensirion SFM3200 Sensor**
 *      Digital Mass FlowMeter from Sensirion AG. Used at beginning for flow control but was *replaced by Pitot/Venturi approach*.
 */

#ifndef _SENSORS_H_
#define _SENSORS_H_

#include <stdint.h>
#include "defaults.h"
#include "calc.h"

#include "src/Honeywell_ABP/Honeywell_ABP.h"
#ifdef ENABLED_SENSOR_VOLUME_SFM3300
#include "src/SFM3200/sfm3000wedo.h"
#endif
#ifdef ENABLED_SENSOR_VOLUME_SDP800
#include "src/sdpsensor/sdpsensor.h"
#endif

#define SENSORS_MAX_ERRORS 5

#if ENABLED_SENSOR_VOLUME_SFM3300
#define SFM3300_OFFSET 32768
#define SFM3300_SCALE 120
#endif

/**
 * @brief Sensor states
 */
enum SensorState
{
    SensorStateOK = 0,
    SensorStateFailed = 1
};

/**
 * @brief Last minimun and maximun pressures
 */
typedef struct
{
    uint8_t minPressure;
    uint8_t maxPressure;
} SensorLastPressure_t;

/**
 * @brief Pressure readings and state
 */
typedef struct
{
    SensorState state;
    float pressure1;
    float pressure2;
} SensorPressureValues_t;

/**
 * @brief Volume readings and state
 */
typedef struct
{
    SensorState state;
    short volume;        // ml
    short instantVolume; // ml
} SensorVolumeValue_t;

class Sensors
{
public:
    Sensors();
    void readPressure(void);
    SensorLastPressure_t getLastPressure(void);
    SensorPressureValues_t getRelativePressure(void);
    SensorVolumeValue_t getVolume(void);
    void saveVolume(void);
    #if ENABLED_SENSOR_VOLUME
        void readFlow(void);
        void resetPressures(void);
        void resetVolumeIntegrator(void);
        float getFlow(void);
        void integrateVolume(void);
    #endif
private:
    Honeywell_ABP *_abp;
    #if ENABLED_SENSOR_VOLUME_SFM3300
        SFM3000wedo *_sfm3000;
        void rebootVolumeSensor(void);
    #endif
    #if ENABLED_SENSOR_VOLUME_SDP800
        SDP8XXSensor *_sdp;
        volatile float _diffPressure;
    #endif
    void _init(void);
    bool stalledVolumeSensor();
    uint8_t _minPressure;
    uint8_t _maxPressure;
    float _pressure1;
    float _pressure2;
    float _pressureSensorsOffset = 0.0;
    SensorState _state;
    SensorState _volumeState;
    byte _errorCounter;
    volatile uint8_t _lastMinPressure;
    volatile uint8_t _lastMaxPressure;
    #if ENABLED_SENSOR_VOLUME
        volatile float _volume_ml;
        volatile float _flow;
        volatile float _lastVolume;
        volatile unsigned long _lastReadFlow;
        volatile unsigned int _flowRepetitionCounter = 0;
    #endif
};

#endif