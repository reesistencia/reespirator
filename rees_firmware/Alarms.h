/** Alarms.
 *
 * @file Alarms.h
 *
 * This is the Alarms software module.
 * It handles the Alarms control loop.
 */
#ifndef INC_ALARMS_H
#define INC_ALARMS_H

#include <float.h>
#include <inttypes.h>
#include "defaults.h"
#include "calc.h"
#include "Sensors.h"

/**
 * @brief Alarm type enumeration
 */
enum Alarm
{
  No_Alarm = 0,
  Alarm_Overpressure = 10,
  Alarm_Pressure_Over_Pip = 11,
  Alarm_No_Pressure = 12,
  Alarm_No_Flow = 20
};

/**
 * This is the Alarms class.
 */
class Alarms
{
public:
  Alarms(Sensors *sensors);

  void evaluatePressure(short pip);
  void evaluateFlow(void);

  void start(void);
  void stop(void);

  void buzzerOn(void);
  void buzzerOff(void);

  void muteAlarm(void);
  void unmuteAlarm(void);

  bool getMutingExpiredFlag(void);
  void setMutingExpiredFlag(bool flag);

  Alarm readAlarm(void);

  void update(short pip);

private:
  /** Initialization. */
  void _init(Sensors *sensors);

  Sensors *_sensors;

  Alarm _currentAlarm = No_Alarm;

  bool _alarmIsMuted = false;
  bool _mutingExpiredFlag = false;

  bool _running = false;

  float _currentPressure = 0.0;
  float _currentFlow = 0.0;
  //float _currentVolume = 0.0;
};

#endif /* INC_ALARMS_H */
