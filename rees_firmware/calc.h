/** Calculation functions
 *
 * @file calc.h
 *
 *
 */
#ifndef CALC_H
#define CALC_H

#include "defaults.h"
#include "Arduino.h"
//#include <math.h>// o <cmath>

/**
 * @brief Sign function utility.
 *
 * Returns -1 if sign is negative or +1 if sign is positive.
 *
 * @tparam T type of value to be evaluated
 * @param val value whose sign is evaluated
 * @return int sign of val
 */
template <typename T> int sign(T val) {
    return (T(0) < val) - (val < T(0));
}

int estimateTidalVolume(int height, int sex);

#endif // CALC_H
