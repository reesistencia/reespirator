/**
 * Sensors reading module
 */
#include "Sensors.h"
#include "defaults.h"
#include "pinout.h"

unsigned int Sensors::begin(void) {
    return 0;
}

Sensors::Sensors(void) {
    _init();
}


void Sensors::_init () {

    _errorCounter = 0;
    _state = SensorStateFailed;

#if ENABLED_SENSOR_VOLUME_SFM3300
    pinMode(20, INPUT_PULLUP);
    pinMode(21, INPUT_PULLUP);
    _sfm3000 = new SFM3000wedo(64);
    _sfm3000->init();
#endif
#if ENABLED_SENSOR_VOLUME
    resetVolumeIntegrator();
#endif
}


#if ENABLED_SENSOR_VOLUME
float Sensors::getFlux(void) {
    return _flux;
}

void Sensors::readVolume(void) {
    #if ENABLED_SENSOR_VOLUME_SFM3300
        SFM3000_Value_t tmp = _sfm3000->getvalue(); //TODO crc
        if (tmp.crcOK) {
            _state = SensorStateOK;
        } else {
            _state = SensorStateFailed;
        }
        float flow = ((float)tmp.value - SFM3300_OFFSET) / SFM3300_SCALE; //lpm
        _flux = flow;
        unsigned short mseconds = (unsigned short)(millis() - _lastReadFlow);
        float ml = flow * mseconds / 60; // l/min * ms * 1000 (ml) /60000 (ms)
        _volume_ml += ml;
        _lastReadFlow = millis();
    #else
    #error "not implemented"
    #endif
}

void Sensors::resetVolumeIntegrator(void) {
    _volume_ml = 0;
    _flux = 0;
    _lastReadFlow = millis();
}
#endif

SensorVolumeValue_t Sensors::getVolume() {
    SensorVolumeValue_t values;

#if ENABLED_SENSOR_VOLUME_SFM3300
    if (_state == SensorStateOK) {
        values.state = SensorStateOK;
    } else {
        values.state = SensorStateFailed;
    }
    values.volume = _volume_ml;
#endif
    return values;
}
